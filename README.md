# web-notification

Tiny notification web component for vanilla JavaScript.

## HTML

Use custom tag below in your HTML. Default-duration attribute (optional) is used to set default duration of notification in ms (default value is 5000; use 0 to disable default auto-hide), animatio-duration attribute (optional) is used to set css animation duration to JavaScript (as delay to removing notification from DOM; default value is 500).
```html
<web-notification default-duration="3000" animation-duration="500"></web-notification>
```

## JavaScript

Link notification.js script into your HTML.

Get element from DOM and call push method. Optional className in parameter is used to additional CSS styling, duration parameter is also optional.
```js
let n = document.querySelector("web-notification");
n.push({message: "test", className: "info", duration: 1000});
```

## CSS
Component does not use shadow DOM because of easy styling,
just edit example CSS however you want.
